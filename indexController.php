<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use Doctrine\DBAL\Types\StringType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


use  AppBundle\Entity\Projet;



class DefaultController extends Controller
{

    /**
     * @Route("/essaie", name="essaie")
     */

    public function testsAction(Request $request)
    {
        $user = $this->getUser();
        $projet=new Projet;
        $form=$this->createFormBuilder($projet)
            ->add('url',TextType::class,array('attr'=>array('class' =>'form-control','style'=>'margin-bottom:15px')))


            ->add('save',SubmitType::class,array('label'=>'Add project','attr'=>array('class' =>'btn btn-primary','style'=>'margin-bottom:15px')))
            ->getForm();
        $form->handleRequest($request);



        if($form->isSubmitted() && $form->isValid()){
            $url=$form['url']->getData();
            $extract = substr(strrchr($url, "/"), 1);
            $extract1 = strstr($extract, '.git', true);
            $result = shell_exec('find /home/jihen/controle/web/' . escapeshellarg($extract1) .' -name "*.py" |xargs pylint  /home/jihen/controle/web/' . escapeshellarg($extract1));
            $filename = substr(strrchr($result, "Your code has been rated at"), 1);

            $out=substr($filename, 0,67);
            if ($out=="") {


                $resultat = shell_exec('php /home/jihen/phpmetrics.phar --report-html=report.html /home/jihen/controle/web' . escapeshellarg($extract1));
                $out = substr(strrchr($resultat, "Maintainability"), 1);


            }


            $url=$form['url']->getData();


            $idUser=$user;
            $projet->setUrl($url);
            $projet->setNote($out);
            $projet->setidUser($idUser);
            $em = $this->getDoctrine()->getManager();
            $em->persist($projet);
            $em->flush();
            exec('git clone '.escapeshellarg($url));
            // echo "<pre>$result</pre>";



            return $this->redirectToRoute('projet');

        }

        return $this->render('todos/Resultat.html.twig',array(

            'form' =>$form->createView(),

        ));
    }

    /**
     * @Route("/todoss", name="homepage")
     */
    public function indexAction(Request $request)
    {


        // $result = shell_exec('find /home/jihen/controle/web/testtest -name "*.py" |xargs pylint /home/jihen/controle/web/testtest');
        //exec('php phpmetrics.phar --version');
        $rslt = shell_exec('php /home/jihen/phpmetrics.phar --report-html=report.html /home/jihen/controle/web/testtest' );
        $rest = substr($rslt,-300);
        // $filename = strrchr($rslt, "Maintainability...................."), 1);

        // $out=substr($filename, 0,900);
        echo "<pre>$rest</pre>";






        return $this->render('todos/test2.html.twig');
    }
    /**
     * @Route("/to/{id}", name="todo_list")
     */

    public function phpAction($id)
    {

        $todo=$this->getDoctrine()
            ->getRepository('AppBundle:Projet')
            ->find($id);



        // $inputValue = $request->get("gender");

        // echo "<pre> $inputValue </pre>";

        // echo $user;
        // if ($_SERVER["REQUEST_METHOD"] === 'POST') {


        //$path = $_POST['url'];

        $filename = substr(strrchr($todo, "/"), 1);
        $filename = substr(strrchr($filename, "/"), 1);
        $user = strstr($filename, '.git', true);


        //if ($inputValue=='php') {


        //    $adr = $_POST['adresse'];

        //    $resul = shell_exec('php /home/jihen/controle/vendor/bin/phpcs --standard=PSR2 /home/jihen/controle/web/' . escapeshellarg($user));

        // $resul = shell_exec('php /home/jihen/controle/vendor/bin/phpcs --standard=PSR2 /home/jihen/controle/web/test');
        //   echo "<pre>$resul</pre>";
        //  }

        // else  if ($inputValue=='python'){


        // $user="python-example-project";


        $result = shell_exec('find /home/jihen/controle/web/' . escapeshellarg($user) .' -name "*.py" |xargs pylint  /home/jihen/controle/web/' . escapeshellarg($user));
        $filename = substr(strrchr($result, "Your code has been rated at"), 1);
        $user = strstr($filename, 'External dependencies', true);
        $out=substr($user, 0,900);
        // echo "<pre>$result</pre>";
        //   }

        //  else {
        //   echo "Veuillez choisir un langage";



        //}
        return $this->render('todos/Analyse.html.twig',array(
            'Resultat'=>$out

        ));
        // array('resul' => $resul));
        //}


    }

    /**
     * @Route("/to", name="todo_list")
     */

    public function analyseAction(Request $request)
    {


        //  $resul = shell_exec('php /home/jihen/controle/vendor/bin/phpcs --standard=PSR2 ' . escapeshellarg($path));

        // $resul = shell_exec('php /home/jihen/controle/vendor/bin/phpcs --standard=PSR2 /home/jihen/controle/web/test');






        $result = shell_exec('find /home/jihen/controle/web/sampleproject -name "*.py" |xargs pylint  /home/jihen/controle/web/sampleproject');
        $filename = substr(strrchr($result, "Your code has been rated at"), 1);
        // $change = strstr($filename, 'External dependencies', true);
        $out=substr($filename, 0,67);

        echo "<pre>$out</pre>";





        return $this->render('todos/test2.html.twig');
        // array('resul' => $resul));
        //}


    }

    public function phpMDAction(Request $request)
    {
        // if($_SERVER["REQUEST_METHOD"]  === 'POST')

        //  {


        //    $adr = $_POST['adresse'];

        //$resul = shell_exec('php /home/jihen/controle/vendor/bin/phpcs --standard=PSR2 /home/jihen/controle/web/'.escapeshellarg($adr));

        $resul = shell_exec('phpmd /home/jihen/controle/src/AppBundle text codesize,unusedcode,naming,design');
        echo "<pre>$resul</pre>";

        return $this->render('todos/Analyse.html.twig');
        // array('resul' => $resul));
        //}


    }
    /**
     * @Route("/create", name="create")
     */
    public function createAction(Request $request){
        $user = $this->getUser();
        $projet=new Projet;
        $form=$this->createFormBuilder($projet)
            ->add('url',TextType::class,array('attr'=>array('class' =>'form-control','style'=>'margin-bottom:15px')))
            ->add('note',IntegerType::class,array('attr'=>array('class' =>'form-control','style'=>'margin-bottom:15px')))

            ->add('save',SubmitType::class,array('label'=>'Add project','attr'=>array('class' =>'btn btn-primary','style'=>'margin-bottom:15px')))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $url=$form['url']->getData();
            $note=$form['note']->getData();
            $idUser=$user;
            $projet->setUrl($url);
            $projet->setNote($note);
            $projet->setidUser($idUser);
            $em = $this->getDoctrine()->getManager();
            $em->persist($projet);
            $em->flush();
            exec('git clone '.escapeshellarg($url) .'/home/jihen/controle/web/download');

            return $this->redirectToRoute('projet');
        }

        return $this->render('todos/Resultat.html.twig',array(

            'form' =>$form->createView()
        ));

    }

    /**
     * @Route("/notify-bitbucket-updates/{repositoryName}")
     */
    public function notifyBitbucketAction(Request $request,$repositoryName){


        $f = fopen("/tmp/$repositoryName.json","w");
        fwrite($f,$request->getContent());
        fclose($f);

        return new Response("ok");
    }



    /**
     * @Route("/projet",name="projet")
     */
    public function listAction(){

        $id = $this->getUser();
        $repository = $this->getDoctrine()->getRepository('AppBundle:Projet');
        $projets = $repository->findByidUser($id);
        // foreach ($projets as $projet){
//$output=$projet->getUrl();
        //  $filename = substr(strrchr($output, "/"), 1);

        // }
        $client = new \GuzzleHttp\Client();
        //  $response = $client->request('GET', 'https://api.bitbucket.org/2.0/repositories/jihenee/testtest');
        $response_master = $client->request('GET', 'https://api.bitbucket.org/2.0/repositories/jihenee/testtest');
        //$data = json_decode($response->getBody()->getContents(), true);
        $jsonContent = json_decode(file_get_contents('https://api.bitbucket.org/2.0/repositories/jihenee/testtest'));
        $jsonContentMaster=json_decode(file_get_contents('https://api.bitbucket.org/2.0/repositories/jihenee/testtest/refs/branches/master'));



        //Mr
        //  $i=0;
        // foreach($projets as $projet){
        //   $filename = substr(strrchr($projet->getUrl(), "/"), 1);
        //  $repoName = strstr($filename, '.git', true);
        // if(file_exists('/tmp/'.$repoName.".json")){
        //    $jsonContent = json_decode(file_get_contents('/tmp/'.$repoName.".json"));
        //   $date = $jsonContent->push->changes[0]->commits[0]->date;
        //   $message = $jsonContent->push->changes[0]->commits[0]->message;
        //  $username=$jsonContent->push->changes[0]->old->target->author->user->username;
        // $projets[$i]->setDate($date);
        // $projets[$i]->setUsername($username);
        //  $projets[$i]->setMessage($message);

        //   }
        //  $i++;
        //  }



        // $Mise_a_jour = "";
        //  $Mise_a_jour='Mise à jour le ' . (new \DateTime($jsonContent['updated_on']))->format('d-m-Y H:i:s');
        $Mise_a_jour=$jsonContent->updated_on;
        $message=$jsonContentMaster->target->message;
        $auteur=$jsonContentMaster->target->author->raw;


        //  $Mise_a_jour='Mise à jour le ' . (new \DateTime($data['pushed_at']))->format('d-m-Y H:i:s');

        // $input=file_get_contents("php://input");

        // $output = json_decode($input, true);

        //  var_dump($input);

        // $username=$input["type"];

        return $this->render('todos/test.html.twig',array(
            'projet'=> $projets,
            'Mise_a_jour'=>$Mise_a_jour,
            'Message'=>$message,
            'Auteur'=>$auteur



        ));
    }

    /**
     * @Route("/modification",name="modification")
     */
    public function modificationAction(){



        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.github.com/repos/jihenee/test');
        $data = json_decode($response->getBody()->getContents(), true);
        $Mise_a_jour='Mise à jour le ' . (new \DateTime($data['pushed_at']))->format('d-m-Y H:i:s');

        return $this->render('todos/Analyse.html.twig',array(

            'Mise_a_jour'=>$Mise_a_jour

        ));
    }


    public function gitAction(Request $request)

    {
        //Je récupère la requete




        // je vérifie si elle est de type POST

        if($_SERVER["REQUEST_METHOD"]  === 'POST')

        {


            $adr = $_POST['adresse']; //////Comme PHP

            //  return new Response(' adresse :'.$adr);}


            exec('git clone '.escapeshellarg($adr) .'/home/jihen/controle/web/download');







            return $this->render('todos/test.html.twig',array('var1' =>$adr));


        }
        else
            return $this->render('todos/test3.html.twig');

    }
    /**
     * @Route("/affiche")
     */
    public function afficheAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('todos/Analyse.html.twig');
    }

    /**
     * @Route("/")
     */

    public function index1Action(Request $request)
    {
        //  $output=file_get_contents("php://input");
        // $data = json_decode($output);
        //$data=json_decode(file_get_contents("php://input"),true);

        //  var_dump(file_get_contents('php://input'));
        // var_dump($data["repository"]);
        // replace this example code with whatever you need
        return $this->render('todos/test3.html.twig');
    }




    /**
     * @Route("/todo/details/{id}",name="todo_details")
     */

    public function details1Action($id)
    {
        $todo=$this->getDoctrine()
            ->getRepository('AppBundle:Projet')
            ->find($id);
        $url=$todo->getUrl();
        $filename = substr(strrchr($url, "/"), 1);

        $user = strstr($filename, '.git', true);
        $result = shell_exec('
         /home/jihen/controle/web/' . escapeshellarg($user) .' -name "*.py" |xargs pylint  /home/jihen/controle/web/' . escapeshellarg($user));
        $filename = substr(strrchr($result, "Your code has been rated at"), 1);
        $user = strstr($filename, 'External dependencies', true);
        $out=substr($user, 0,900);
        if ($out=="") {


            $resultat = shell_exec('php /home/jihen/phpmetrics.phar --report-html=report.html /home/jihen/controle/web' . escapeshellarg($user));
            $out = substr($resultat,-300);}

        echo "<pre>$out</pre>";

        return $this->render('todos/test2.html.twig');
    }








}
?>